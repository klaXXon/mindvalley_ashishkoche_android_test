package com.ashishk.mindvalley_ashishkoche_android_test;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements AbsListView.OnItemClickListener {

    private StaggeredGridView mGridView;
    private DataAdapter mAdapter;
    public static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Pinterest Layout Demo");
    }

    @Override
    protected void onStart(){
        super.onStart();
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        mAdapter = new DataAdapter(this, R.layout.list_item_sample, generateSampleData());
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
    }

    public static ArrayList<Data> generateSampleData() {

        final ArrayList<Data> dataArrayList = new ArrayList<Data>();

        try {
            JSONArray jsonArray = new JSONArray(UrlJsonData.strJson);

            for(int i=0; i<jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                JSONObject userInfo = jsonObject.getJSONObject("user");
                String name = userInfo.optString("name").toString();

                JSONObject profileImage = userInfo.getJSONObject("profile_image");
                String largeImageUrl = profileImage.optString("large");

                Data data1 = new Data(largeImageUrl, name, "1 This is Just Super Awesome");
                dataArrayList.add(data1);

                JSONObject jsonUrls = jsonObject.getJSONObject("urls");
                String smallUrl = jsonUrls.optString("small");

                Data data2 = new Data(smallUrl, name, "2 This is Just Super Awesome");
                dataArrayList.add(data2);

                JSONObject jsonLinks = jsonObject.getJSONObject("links");
                String downloads = jsonLinks.optString("download");

                Data data3 = new Data(downloads, name, "3 This is Just Super Awesome");
                dataArrayList.add(data3);

                Log.i(TAG, i + " id : " + " name : " + name + " large : " + largeImageUrl
                        + " small : " + smallUrl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataArrayList;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Toast.makeText(this, "Item Clicked: " + position, Toast.LENGTH_SHORT).show();
    }
}