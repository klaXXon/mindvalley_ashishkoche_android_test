package com.ashishk.mindvalley_ashishkoche_android_test;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.ashishk.datacacherlibrary.interfaces.IResourceHandler;
import com.ashishk.datacacherlibrary.utils.DataManager;
import com.etsy.android.grid.util.DynamicHeightImageView;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by akoche on 10/15/2016.
 */
public class DataAdapter extends ArrayAdapter<Data> {

    Activity activity;
    int resource;
    List<Data> datas;
    public static final String TAG = "DataAdapter";
    private Context mContext;

    public DataAdapter(Activity activity, int resource, List<Data> objects) {
        super(activity, resource, objects);
        this.activity = activity;
        this.resource = resource;
        this.datas = objects;
        this.mContext = this.getContext();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        View viewtp = null;
        final DealHolder holder;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new DealHolder();
            holder.image = (DynamicHeightImageView)row.findViewById(R.id.image);
            holder.title = (TextView)row.findViewById(R.id.title);
            holder.description = (TextView)row.findViewById(R.id.description);
            row.setTag(holder);
        } else {
            holder = (DealHolder) row.getTag();
        }

        final Data data = datas.get(position);

        DataManager.getInstance(mContext).getResourceManager().loadResource(data.imageUrl, new IResourceHandler() {
            @Override
            public void resourceLoadedFromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes){
                setImage(bytes);
            }

            @Override
            public void resourceLoadedFromCache(byte[] bytes) {
                setImage(bytes);
            }

            @Override
            public void resourceLoadedFromLocal(byte[] bytes) {
                setImage(bytes);
            }

            public void setImage(byte[] bytes) {
                int finalHeight = holder.image.getMeasuredHeight();
                int finalWidth = holder.image.getMeasuredWidth();
                Bitmap bitmap = DataManager.getInstance(mContext).getTransformers().ConvertBytestoBitmap(bytes, finalWidth, finalHeight);
                holder.image.setImageBitmap(bitmap);
            }

        }, viewtp);

        holder.image.setHeightRatio(1.0);
        holder.title.setText(data.title);
        holder.description.setText(data.description);

        return row;
    }

    static class DealHolder {
        DynamicHeightImageView image;
        TextView title;
        TextView description;
    }
}