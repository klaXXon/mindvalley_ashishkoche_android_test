package com.ashishk.mindvalley_ashishkoche_android_test;

/**
 * Created by akoche on 10/16/2016.
 */
public class Data {
    public String imageUrl;
    public String title;
    public String description;
    public Data() {}

    public Data (String imageUrl, String title, String description) {
        this.imageUrl = imageUrl;
        this.title = title;
        this.description = description;
    }
}
