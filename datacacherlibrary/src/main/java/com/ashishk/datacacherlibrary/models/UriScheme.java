package com.ashishk.datacacherlibrary.models;

/**
 * Created by akoche on 10/13/2016.
 */
public class UriScheme {

    public String uri;
    public String contentType;

    public UriScheme(String uri, String contentType){
        this.uri=uri;
        this.contentType=contentType;
    }
}
