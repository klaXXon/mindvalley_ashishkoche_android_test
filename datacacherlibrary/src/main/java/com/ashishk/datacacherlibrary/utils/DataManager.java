package com.ashishk.datacacherlibrary.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.Toast;

import com.ashishk.datacacherlibrary.interfaces.IResourceHandler;
import com.ashishk.datacacherlibrary.models.UriScheme;
import com.ashishk.datacacherlibrary.utils.ServiceBuilder;
import com.ashishk.datacacherlibrary.interfaces.IRetrofitResourceApi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by akoche on 10/12/2016.
 */
public class DataManager {

    /* Single instance is used throughout the library */
    private static DataManager mInstance;
    private Context mContext;
    int maxMemoryUsed;
    private Transformers mTransformers;
    private Gson mGson;
    private LruCache<String, byte[]> mLruCache;
    private static ResourceManager mResourceManager;

    public DataManager(Context context) {
        this.mContext = context;
        this.maxMemoryUsed = (int) (Runtime.getRuntime().maxMemory() / 1024)/4;
        this.mTransformers=new Transformers();
        mGson = new Gson();
        initLruCache(maxMemoryUsed);
    }

    public static DataManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DataManager(context);
        }
        return mInstance;
    }

    private void initLruCache(int maxMemory) {
        mLruCache = new LruCache<String, byte[]>(maxMemory){
        };
    }

    public Transformers getTransformers() {
        return this.mTransformers;
    }

    public ResourceManager getResourceManager(){
        if(mResourceManager==null)
            mResourceManager= new ResourceManager();
        return mResourceManager;
    }


    private String findCacheKeyFromUri(String uri) {
        Map<String, byte[]> snapshot = mLruCache.snapshot();
        String key = null;
        for (String id : snapshot.keySet()) {
            Type deserializeType = new TypeToken<UriScheme>() {
            }.getType();
            UriScheme deserialized = mGson.fromJson(id, deserializeType);
            if (deserialized.uri.equals(uri)) {
                key=id;
                break;
            }
        }
        return key;
    }

    public byte[] getFromMemoryByKey(String key) {
        byte[] bytes = mLruCache.get(key);
        return bytes;
    }


    public byte[] loadFromMemory(String url) {
        String key = findCacheKeyFromUri(url);
        if (TextUtils.isEmpty(key))
            return null;
        return getFromMemoryByKey(key);
    }
    ////////////
    public void saveInMemoryCache(String url, byte[] bytes) {
        saveInMemoryCacheByKey(url, bytes);
    }

    public void saveInMemoryCacheByKey(String key, byte[] bytes) {
        mLruCache.put(key, bytes);
    }
    // Above two functions can be combined

    public class ResourceManager{
        public static final String TAG = "ResourceManager";

        public byte[] loadResourceFromCache(String uri){
            byte[] bytes= loadFromMemory(uri);
            return bytes;
        }

        public void loadResource(String uri, final IResourceHandler handler, View cancel) {
            byte[] bytes = loadResourceFromCache(uri);
            if (bytes != null) {
                handler.resourceLoadedFromCache(bytes);
                return;
            }
            loadResourceFromRemote(uri, handler, cancel);
        }

        public void loadResource(int resource, final IResourceHandler handler, View cancel) {
            byte[] bytes = loadResourceFromCache(String.valueOf(resource));
            if (bytes!=null) {
                handler.resourceLoadedFromCache(bytes);
                return;
            }
            loadResourceFromLocal(resource, handler);
        }

        public Call loadResourceFromRemote(String uri, final IResourceHandler handler,View cancel){
            ServiceBuilder builder = new ServiceBuilder();
            IRetrofitResourceApi api= builder.createService(IRetrofitResourceApi.class);
            final Call<ResponseBody> call= api.fetchResource(uri);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    byte[] bytes=  updateCacheMemory(call,response);
                    handler.resourceLoadedFromRemote(call, response, bytes);
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if (t != null) {
                        Log.e(TAG, t.getMessage());
                        Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            if(cancel!=null) {
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        call.cancel();
                    }
                });
            }
            return call;
        }

        public byte[] updateCacheMemory(Call<ResponseBody> call, Response<ResponseBody> response) {
            byte[] bytes=null;
            String uri=  call.request().url().toString();
            String contentType= response.headers().get("content-type");
            UriScheme scheme= new UriScheme(uri,contentType);
            String serializedScheme= mGson.toJson(scheme);
            try {
                bytes= response.body().bytes();
                saveInMemoryCache(serializedScheme, bytes);
                Log.i(TAG, serializedScheme);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bytes;
        }

        private void loadResourceFromLocal(int resId, IResourceHandler handler) {
            Drawable r = mContext.getResources().getDrawable(resId);
            Bitmap bitmap = ((BitmapDrawable) r).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
            byte[] bytes = stream.toByteArray();
            UriScheme scheme = new UriScheme(String.valueOf(resId), "drawable");
            saveInMemoryCache(mGson.toJson(scheme), bytes);
            handler.resourceLoadedFromLocal(bytes);
        }
    }

}