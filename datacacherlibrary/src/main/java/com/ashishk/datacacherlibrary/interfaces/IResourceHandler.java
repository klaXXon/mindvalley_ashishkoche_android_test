package com.ashishk.datacacherlibrary.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by akoche on 10/13/2016.
 */
public interface IResourceHandler {
    public void resourceLoadedFromCache(byte[] bytes);
    public void resourceLoadedFromRemote(Call<ResponseBody> call, Response<ResponseBody> response, byte[] bytes);
    public void resourceLoadedFromLocal(byte[] bytes);
}
