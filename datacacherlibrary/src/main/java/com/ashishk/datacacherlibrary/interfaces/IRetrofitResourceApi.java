package com.ashishk.datacacherlibrary.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by akoche on 10/13/2016.
 */
public interface IRetrofitResourceApi {
    @GET
    Call<ResponseBody> fetchResource(@Url String url);
}